# DOCKER MYSQL WORKBENCH


## MENU

* **The Package**
    + [Why Exists?](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/why_exists.md)
    + [What Is It?](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/what_is_it.md)
    + [When To use It?](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/when_to_use_it.md)
* **How To**
    + [Install](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/install.md)
    + [Use](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/use.md)
    + [Report an Issue](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/create_an_issue.md)
    + [Create a Branch](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/create_branches.md)
    + [Open a Merge Request](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/create_a_merge_request.md)
    + [Uninstall](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/how-to/uninstall.md)
* **Demos**
    + [Mysql Workbench](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/demos/mysql-workbench.md)
* **Road Map**
    + [Milestones](https://gitlab.com/exadra37-docker/mysql/workbench/milestones)
    + [Overview](https://gitlab.com/exadra37-docker/mysql/workbench/boards)
* **About**
    + [Author](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/AUTHOR.md)
    + [Contributors](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/CONTRIBUTORS.md)
    + [Contributing](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/CONTRIBUTING.md)
    + [License](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/LICENSE)


## SUPPORT DEVELOPMENT

If this is useful for you, please:

* Share it on [Twitter](https://twitter.com/home?status=Using%20%23Mysql%20Workbench%20from%20a%20%23docker%20%23container%20by%20%40Exadra37%20https%3A//gitlab.com/exadra37-docker/mysql/workench.%20%23developers%20%23mysqlworkbench%20%23devops%20%23sysadmins) or in any other channel of your preference.
* Consider to [offer me](https://www.paypal.me/exadra37) a coffee, a beer, a dinner or any other treat 😎.


## EXPLICIT VERSIONING

This repository uses [Explicit Versioning](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## BRANCHES

Branches are created as demonstrated [here](docs/how-to/create_branches.md).

This are the type of branches we can see at any moment in the repository:

* `master` - Issues and milestones branches will be merged here. Don't use it in
              production.
* `latest` - Matches the last stable tag created. Useful for automation tools.
             Doesn't guarantee backwards compatibility.
* `4-fix-some-bug` - Each issue will have is own branch for development.
* `milestone-12_add-some-new-feature` - all Milestone issues will start, tracked and merged
                             here.

Only `master` and `latest` branches will be permanent ones in the repository and
all other ones will be removed once they are merged.


## DISCLAIMER

I code for passion and when coding I like to do as it pleases me...

You know I do this in my free time, thus I want to have fun and enjoy it ;).

Professionally I will do it as per company guidelines and standards.
