# WHAT IS IT?

A project to allow us to:

* build a Docker Image for Mysql Workbench graphical user interface.
* use docker compose to run it from a docker container.
* try new or old versions while keeping the current version untouched.

---

[<< previous](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/why_exists.md) | [next >>](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/when_to_use_it.md)

[HOME](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/README.md)

